QT += charts qml quick
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp
QT       += core gui charts
QT += charts
RESOURCES += \
    qml.qrc


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = /home/ian/QT/stb-ihm/IHM/imports

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
DISTFILES += \
    imagens/logo.png \
    imagens/qt-logo.png \
    imagens/ic_add_alert_black_24px.svg \
    imagens/ic_apps_white_24px.svg \
    imagens/ic_arrow_back_white_24px.svg \
    imagens/ic_chevron_left_white_24px.svg \
    imagens/ic_chevron_right_white_24px.svg \
    imagens/ic_developer_board_white_48px.svg \
    imagens/ic_home_black_24px.svg \
    imagens/ic_memory_white_48px.svg \
    imagens/ic_menu_white_24px.svg \
    imagens/ic_more_vert_white_24px.svg \
    imagens/ic_network_check_white_48px.svg \
    imagens/ic_settings_black_24px.svg \
    imagens/ic_storage_black_24px.svg \
    imagens/ic_warning_black_24px.svg \
    imagens/ic_warning_white_48px.svg \
    imagens/ic_wifi_tethering_white_48px.svg
