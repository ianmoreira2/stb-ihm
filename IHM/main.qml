import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4 as Qc14
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: window
    visible: true
    width:800
    height: 480
    title: qsTr("STB-IHM")

    StackView {
        id: stackView
        initialItem: "Login.qml"
        anchors.fill: parent

    }
}
