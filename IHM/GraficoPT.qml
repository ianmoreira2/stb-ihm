import QtQuick 2.0
import QtCharts 2.2


Item {
    anchors.fill: parent

    //![1]
    ChartView {
        title: "Spline"
        anchors.fill: parent
        antialiasing: true
        theme: ChartView.ChartThemeDark
//        backgroundColor: "#333333"
        animationOptions: ChartView.GridAxisAnimations

        ValueAxis {
                id: axisX
                min: 0
                max: 10
                tickCount: 5
            }

            ValueAxis {
                id: axisY
                min: 0
                max: 12
            }

        SplineSeries {
            name: "Yellow"
            XYPoint { x: 0; y: 0.0 }
            XYPoint { x: 1.1; y: 3.2 }
            XYPoint { x: 4.9; y: 4.4 }
            XYPoint { x: 6.1; y: 5.1 }
            XYPoint { x: 8.9; y: 7.6 }
            XYPoint { x: 9.4; y: 8.3 }
            XYPoint { x: 10.1; y: 10.1 }
            color: "yellow"
            width: 3
            capStyle: Qt.FlatCap
            axisX: axisX
            axisY: axisY
        }
        SplineSeries {
            name: "Red"
            XYPoint { x: 0; y: 0.0 }
            XYPoint { x: 1.1; y: 1.2 }
            XYPoint { x: 3.9; y: 1.4 }
            XYPoint { x: 5.1; y: 2.1 }
            XYPoint { x: 6.9; y: 3.6 }
            XYPoint { x: 7.4; y: 4.3 }
            XYPoint { x: 10.1; y: 5.1 }
            capStyle: Qt.SquareCap
            width: 3
            color: "red"
            style: Qt.DotLine
        }
    }
}
