import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4 as QuickCon14
import QtQuick.Controls.Styles 1.4
import QtCharts 2.2
import QtQuick.Extras 1.4
import QtQuick.Dialogs 1.3

Page {
    id: page
    property int warning: 1
    property date startTime : new Date()
    property int  on: 1
    anchors.fill: parent
    background: Rectangle {
        anchors.fill: parent
        color: "#303030"
    }

    Column {

        spacing: 20
        RowLayout {
            spacing: 5

            Rectangle {
                width: 236
                height: 125
                color: "#444444"
                Layout.fillWidth: true

                ColumnLayout {
                    id: column1
                    spacing: 3
                    anchors.fill: parent

                    Row {
                        width: 174
                        spacing: 0
                        Rectangle{
                            width: 127
                            height: 94
                            color: "white"
                            Image {
                                id: image
                                width: 127
                                height: 92
                                sourceSize.height: 0
                                sourceSize.width: 0
                                fillMode: Image.PreserveAspectFit
                                source: "imagens/logo2.png"
                            }
                        }
                        Column{
                            spacing: 3
                            Row {
                                width: 108
                                spacing: 2

                                ItemDelegate {
                                    id: user
                                    width: 53
                                    height: 45
                                    font.family: "Arial"
                                    focusPolicy: Qt.WheelFocus
                                    background: Rectangle {
                                        implicitWidth: 20
                                        implicitHeight: 20
                                        opacity: enabled ? 1 : 0.3
                                        color: user.down ? "#424242" : "#555555"
                                    }
                                    Image {

                                        anchors.horizontalCenter: parent.horizontalCenter
                                        anchors.verticalCenter: parent.verticalCenter
                                        horizontalAlignment: Qt.AlignHCenter
                                        verticalAlignment: Image.AlignVCenter

                                        source: "imagens/ic_account_circle_white_36px.svg"
                                    }
                                }

                                ItemDelegate {
                                    id: restart
                                    width: 53
                                    height: 45
                                    background: Rectangle {
                                        implicitWidth: 20
                                        implicitHeight: 20

                                        opacity: enabled ? 1 : 0.3
                                        color: restart.down ? "#424242" : "#555555"
                                    }
                                    Image {
                                        horizontalAlignment: Qt.AlignHCenter
                                        verticalAlignment: Image.AlignVCenter
                                        id: logo
                                        anchors.verticalCenter: parent.verticalCenter
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        source: "imagens/ic_cached_red_36px.png"
                                    }
                                }
                            }
                            ItemDelegate{
                                id: col
                                width: 107
                                height: 45
                                background: Rectangle {
                                    implicitWidth: 107
                                    implicitHeight: 45
                                    opacity: enabled ? 1 : 0.3
                                    color: col.down ? "#424242" : "#555555"
                                }
                                Image {
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    anchors.verticalCenter: parent.verticalCenter
                                    source: "imagens/ic_tv_white_36px.svg"
                                }
                            }
                        }
                    }

                    Rectangle{
                        id: rectangle1
                        width: 236
                        height: 30
                        radius: 2
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 0




                        Label{
                            text: "MODEL"
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            font.pointSize: 12
                            font.bold: true
                            horizontalAlignment: Text.AlignHCenter
                        }

                    }
                }
            }

            Rectangle {
                width: 312
                height: 126
                color: "#393939"

                RowLayout {
                    id: rowLayout1
                    anchors.fill: parent

                    ColumnLayout {
                        y: 0
                        width: 94
                        height: 125
                        Layout.fillHeight: false
                        Layout.fillWidth: false
                        spacing: 0
                        Rectangle {
                            width: 90
                            height: 31
                            color: "#424242"
                            Layout.fillWidth: true
                            Text {
                                id: text1
                                text: qsTr("Set Point")
                                anchors.fill: parent
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                font.pixelSize: 16
                                color: "white"
                            }
                        }

                        Rectangle {
                            color: "#ffffff"
                            opacity: 1
                            Layout.fillWidth: true
                            width: 85
                            height: 30
                            Text {
                                width: 90
                                height: 30
                                color: "#363636"

                                text: "720 MHZ"
                                elide: Text.ElideNone
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                wrapMode: Text.NoWrap
                                font.bold: enabled
                                font.pixelSize: 14
                            }
                        }
                        Rectangle {
                            id: rectangle3
                            width: 90
                            height: 31
                            color: "#424242"
                            Layout.fillWidth: true
                            Text {
                                width: 94
                                height: 24
                                leftPadding: 10
                                text: qsTr("Frequency")
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.top: parent.top
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                font.pixelSize: 16
                                color: "white"
                            }
                        }

                        Rectangle {
                            color: "#ffffff"
                            Layout.fillWidth: true
                            width: 85
                            height: 30
                            Text {
                                width: 94
                                height: 30
                                color: "#363636"
                                padding: 5
                                text: "720 MHZ"
                                elide: Text.ElideNone
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                wrapMode: Text.NoWrap
                                font.bold: enabled
                                font.pixelSize: 14
                            }
                        }
                    }

                    ColumnLayout {
                        x: 159
                        width: 212
                        spacing: 5
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        Rectangle {
                            width: 90
                            height: 31
                            color: "#424242"
                            Layout.fillWidth: true
                            Text {

                                text: qsTr("TRANSMITTER")
                                anchors.fill: parent
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                font.pixelSize: 16
                                color: "white"
                            }

                        }
                        Rectangle {
                            id: rectangle2
                            width: 214
                            height: 53
                            color: "#ffffff"
                            Layout.fillWidth: true
                            Text {
                                width: 214
                                height: 53

                                text: qsTr("XXX31.2 kW")
                                font.bold: true
                                anchors.fill: parent
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                font.pixelSize: 22
                                color: "#000000"
                            }

                        }

                        RowLayout{
                            id: rowLayout2
                            Layout.fillWidth: true
                            Label{
                                id: label1
                                text:"REFLECTED"
                                font.pointSize: 11
                                Layout.fillWidth: false
                                color: "white"
                            }
                            Rectangle{
                                id: rectangle
                                color: "#e4e4e4"
                                Layout.fillHeight: true
                                Layout.fillWidth: true

                                Label{
                                    text:"5 W"
                                    font.pointSize: 14
                                    font.bold: true
                                    verticalAlignment: Text.AlignVCenter
                                    anchors.fill: parent
                                    horizontalAlignment: Text.AlignHCenter


                                }
                            }
                        }
                    }
                }


            }

            Rectangle {
                width: 238
                height: 125
                color: "#393939"

                QuickCon14.ToolBar {
                    id: toolBar
                    height: 23
                    style: ToolBarStyle {

                        background: Rectangle {
                            radius: 3
                            implicitWidth: 238
                            color: "#3D3D3D"
                        }
                    }

                    ItemDelegate{
                        width: 23
                        height: 23
                        Layout.fillWidth: false
                        Layout.fillHeight: true
                        onClicked: contextMenu.popup()
                        Image {
                            source: "imagens/ic_menu_white_24px.svg"
                        }
                    }
                    Label {
                        id: label

                        color: "#ffffff"
                        text: qsTr("Exciter")
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        font.pointSize: 12
                    }

                }

                ScrollView {
                    id: scrollView
                    contentHeight: 150
                    wheelEnabled: true
                    hoverEnabled: true
                    visible: true
                    anchors.topMargin: 24
                    anchors.fill: parent
                    clip: true

                    ColumnLayout{
                        id: columnLayout
                        width: parent.width
                        spacing: 2

                        ProgressBarCustom {
                            radius: 2
                            progressColor: "#0CD255"
                            progressValue: 0.5
                             Layout.fillWidth: true
                            height: 47
                        }

                        ProgressBarCustom {
                            radius: 2
                            progressColor: "#F30F0F"
                            progressValue: 0.6
                            height: 47
                        }

                    }
                }
            }
        }

        RowLayout {
            spacing: 5

            Rectangle {
                width: 236
                height: 257
                radius: 1
                color: "#5F5F5F"
                QuickCon14.ToolBar {
                    height: 24
                    width: parent.width
                    style: ToolBarStyle {

                        background: Rectangle {
                            radius: 3
                            implicitWidth: 238
                            implicitHeight: 20
                            color: "#424242"
                        }
                    }

                    ItemDelegate{
                        width: 23
                        height: 23
                        onClicked: contextMenu.popup()
                        Image {
                            source: "imagens/ic_menu_white_24px.svg"
                        }
                    }
                    Label {
                        anchors.fill: parent
                        text: "Meters"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Qt.AlignHCenter
                        color: "white"

                        font.pixelSize: 14
                    }
                }
                ScrollView{
                    x: 0
                    y: 24
                   anchors.fill: parent
                    wheelEnabled: true
                    hoverEnabled: true
                    anchors.topMargin: 25
                    clip: true

                    ColumnLayout {
                        width: parent.width
                        spacing: 2
                        height: 236


                        ProgressBarCustom {
                            width: parent.width
                            radius: 3
                            Layout.fillWidth: true
                            progressColor: "#0CD255"
                            progressValue: 0.6
                        }

                        ProgressBarCustom {
                            progressColor: "#F30f0f"
                            progressValue: 0.1
                        }

                        ProgressBarCustom {
                            progressColor: "#FFF021"
                            progressValue: 0.3
                        }

                        ProgressBarCustom {
                            progressColor: "#0CD255"
                            progressValue: 0.5
                        }
                        ProgressBarCustom {
                            progressColor: "#0CD255"
                            progressValue: 0.5
                        }

                    }
                }
            }


            Rectangle {
                width: 557
                height: 257
                color: "#303030"

                QuickCon14.ToolBar {

                    Layout.fillWidth: true
                    style: ToolBarStyle {

                        background: Rectangle {
                            radius: 1
                            implicitWidth: 238
                            implicitHeight: 20
                            color: "#424242"
                        }
                    }

                    ItemDelegate{
                        width: 23
                        height: 23
                        onClicked: contextMenu.popup()
                        Image {
                            source: "imagens/ic_menu_white_24px.svg"
                        }
                    }

                    Label {
                        text: "Gráfico de Potência dos Módulos"
                        anchors.fill: parent
                        verticalAlignment: Text.AlignVCenter
                        Layout.fillHeight: true
                        horizontalAlignment: Qt.AlignHCenter
                        Layout.fillWidth: true
                        color: "white"
                        font.pixelSize: 14
                    }
                }

                StackView{
                    id: stackChart
                    anchors.bottomMargin: 0
                    anchors.topMargin: 24
                    anchors.fill: parent
                    initialItem: main

                }
                Component{
                    id:main
                    ChartView {
                        backgroundRoundness : 3
                        anchors.fill: parent
                        //                    legend.alignment: Qt.AlignBottom
                        antialiasing: true
//                        plotAreaColor: "#303030"
                        theme: ChartView.ChartThemeDark
                        animationOptions: ChartView.AllAnimations
                        ValueAxis {
                            id: axisX
                            min: 0
                            max: 10
                            tickCount: 5
                        }

                        ValueAxis {
                            id: axisY
                            min: 400
                            max: 1000
                        }
                        StackedBarSeries {
                            id: mySeries
                            axisY: axisY
                            axisX: BarCategoryAxis { categories: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10" ] }
                            BarSet { label: "M1"; values: [450, 600, 700, 400, 500, 600, 600, 600, 400, 500, 600] }
                            BarSet { label: "M2"; values: [50, 40, 60, 70, 80, 70, 60, 90, 40, 50, 60] }
                            BarSet { label: "M3"; values: [60, 50, 80, 70, 50, 80, 60, 90, 40, 50, 60] }
                        }
                    }
                }

            }
        }
    }
    Menu {
        id: contextMenu
        contentHeight: 0.1
        x: 25
        y: 25
        background: Rectangle {

            implicitWidth: 150
            implicitHeight: 50
            color: "#aaa9a9"
            border.color: "#21be2b"
            radius: 2
            opacity: 0.99
        }

        MenuItem {
            text: "Temperature"

            onClicked: stackChart.push("GraficoPT.qml", StackView.Immediate)
        }
        MenuItem {
            text: "Exciter"
            onClicked: stackChart.pop(StackView.Immediate)
        }
        MenuItem { text: "reflected" }
    }
    Dialog {
        id: dialog
//        width: 200
//        height: 200
        title: "Title"

        standardButtons: Dialog.Ok
        onAccepted: {
            tAlarme.running = false
            console.log("Ok clicked")
        }
        onRejected: console.log("Cancel clicked")
        Text {
            text: qsTr("Há módulos com mau funcionamento.")
        }
    }

    MessageDialog {
        id: messageDialog
        icon: StandardIcon.Warning
        title: "Alerta do módulo"
        text: "Atenção! Modulo XXXXX pode estar com defeito."
        onAccepted: {
            tAlarme.running = false
//            Qt.quit()
        }
    }


    footer: QuickCon14.ToolBar{
        style: ToolBarStyle {
            background: Rectangle {
                color: "#333333"
                implicitWidth: 800
                implicitHeight: 60
            }
        }
        RowLayout{
            x: 0
            spacing: 1
            anchors.rightMargin: -6
            anchors.topMargin: 0
            anchors.bottomMargin: -7
            anchors.leftMargin: -5
            anchors.fill: parent


            ItemDelegate {
                id: control
                width: 111
                height: 60

                onClicked: {
//                    stackView.push("Home.qml",StackView.Immediate)
                    on = !on

                }
                background: Rectangle {
                    implicitWidth: 111
                    implicitHeight: 60

                    opacity: enabled ? 1 : 0.3
                    color: control.down ? "#777777" : "#424242"

                }
                RowLayout{
                    x: 0
                    anchors.fill: parent

                    spacing:10
                    Image {
                        id: name
                        fillMode: Image.PreserveAspectFit
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillHeight: false
                        Layout.fillWidth: false
                        source: on ? "imagens/ic_wifi_tethering_green_48px.png" : "imagens/ic_wifi_tethering_red_48px.png"
                    }
                    Text {
                        text: qsTr("ON")
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 20
                        color: "white"
                        font.bold: enabled
                        Layout.alignment: Text.AlignHCenter
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                    }

                }
            }

            ItemDelegate {
                id:alarme
                width: 202
                height: 60
                onClicked: {
                    dialog.open()

                }
                Timer{
                    id:tAlarme
                    interval: 500
                    running: true
                    repeat: true
                    onTriggered: {
                         warning =!warning
                    }
                }

                background: Rectangle {
                    implicitWidth: 202
                    implicitHeight: 60


                    opacity: enabled ? 1 : 0.3
                    color: alarme.down ? "#777777" : "#424242"

                }
                RowLayout{
                    width: 180
                    height: 50
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.verticalCenter: parent.verticalCenter

                    spacing:10
                    Image {
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        source: warning ?  "imagens/ic_report_problem_white_36px.svg" :"imagens/ic_report_problem_yellow_36px.png"
                    }
                    Text {
                        height: 55
                        text: qsTr("Alarme 1 XXXX\nAlarme 1 XXXX\nAlarme 1 XXXX")
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        font.italic: enabled
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 14
                        color: "white"
                        elide: Text.ElideRight
                    }
                }
            }

            ItemDelegate {
                id:configuracao
                width: 155
                height: 60
                background: Rectangle {
                    implicitWidth: 155
                    implicitHeight: 60
                    opacity: enabled ? 1 : 0.3
                    color: configuracao.down ? "#777777" : "#424242"

                }
                RowLayout{
                    anchors.fill: parent

                    spacing:10
                    Image {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: false
                        source: "imagens/ic_settings_white_48px.svg"
                    }
                    Text {
                        text: qsTr("Settings")
                        font.bold: true
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 14
                        color: "white"
                        elide: Text.ElideRight
                    }
                }
            }

            ItemDelegate {
                id:gerenciamento
                width: 155
                height: 60
                background: Rectangle {
                    implicitWidth: 155
                    implicitHeight: 60
                    opacity: enabled ? 1 : 0.3
                    color: gerenciamento.down ? "#777777" : "#424242"

                }
                RowLayout{
                    anchors.fill: parent
                    spacing:10
                    Image {
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        source: "imagens/ic_build_white_36px.svg"
                    }
                    Text {
                        text: qsTr("Management")
                        font.bold: true
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        font.pixelSize: 14
                        verticalAlignment: Text.AlignHCenter
                        color: "white"
                        elide: Text.ElideRight
                    }
                }
            }

            ItemDelegate {
                id:casa
                width: 160
                height: 60
                background: Rectangle {
                    implicitWidth: 160
                    implicitHeight: 60
                    color: "#424242"
                }

                Timer {
                    interval: 500;
                    running: true;
                    repeat: true

                    onTriggered: {

                        var current = new Date()
                        var delta =  new Date(current - startTime)
                        time.text = Qt.formatDateTime(current,"hh:mm\n ddd dd/MM/yyyy\n").concat(Qt.formatDateTime(delta," 00:mm:ss"))
                    }
                }

                Text {
                    id: time
                    padding: 10
//                    text: Qt.formatDateTime(current,"hh:mm\n ddd dd/MM/yyyy\n").concat(Qt.formatDateTime(delta," 00:mm:ss"))
                    verticalAlignment: Text.AlignVCenter
                    anchors.fill: parent
                    lineHeight: 1
                    renderType: Text.NativeRendering
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 14
                    color: "white"
                    elide: Text.ElideNone
                }
            }
        }
    }



}
