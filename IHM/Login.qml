import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4 as QuickCon14
import QtQuick.Controls.Material 2.2
import QtQuick.Controls.Styles 1.4
import QtQml 2.2

Page {
    property string title_header: "Login"
    width: 800
    height: 480
    property alias control: control
    visible: true
    header: QuickCon14.ToolBar {
        style: ToolBarStyle {
                padding {
                    left: 8
                    right: 8
                    top: 3
                    bottom: 3
                }
                background: Rectangle {
                    color: "#2B343B"
                    implicitWidth: 100
                    implicitHeight: 40
                }
            }
        RowLayout {
            anchors.fill: parent

            Label {
                id: title_toolbar
                leftPadding: 40
                font.bold: enabled
                text: title_header
                font.pixelSize: 18
                color: "white"
                horizontalAlignment: Qt.AlignLeft
                Layout.fillWidth: true
            }

        }
    }


    Rectangle{
        id: rectangle
        anchors.fill: parent
        color: "#333333"

        ColumnLayout{
            anchors.fill: parent
            anchors.right: parent.right
            spacing: 1
            Label {
                color: "#019786"
                text: "DTU"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                font.bold: true
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 40
            }
            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Seja bem vindo! Por favor entre com a sua conta"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                font.pointSize: 13
                Layout.fillWidth: false
                horizontalAlignment: Text.AlignHCenter
                color: "#ced6d6"
            }

            QuickCon14.TextField {
                id: usuario_login
                implicitWidth: 450
                anchors.horizontalCenter: parent.horizontalCenter
                placeholderText: qsTr("Usuário")
                style: TextFieldStyle {
                          textColor: "#019786"
                          placeholderTextColor: "#019786"
                          background: Rectangle {
                              radius: 3
                              color: "#333333"
                              implicitWidth: 100
                              implicitHeight: 40
                              border.color: "#019786"
                              border.width: 1
                          }
                      }
            }
            QuickCon14.TextField {
                anchors.horizontalCenter: parent.horizontalCenter
                implicitWidth: 450
                id: control
                width: 450
                height: 40
                echoMode: 2
                placeholderText: qsTr("Senha")
                style: TextFieldStyle {
                          passwordCharacter: TextInput.PasswordCharacter
                          textColor: "#019786"
                          placeholderTextColor: "#019786"
                          background: Rectangle {
                              radius: 3
                              color: "#333333"
                              implicitWidth: 100
                              implicitHeight: 40
                              border.color: "#019786"
                              border.width: 1
                          }
                      }
            }

            Button{
                anchors.left: parent.horizontalCenter
                text: "Esqueci a senha"
                spacing: 5
                font.bold: false
                highlighted: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12

            }

            Button{
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Login"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                highlighted: true
                onClicked: stackView.push("Home.qml")

            }

        }

    }

    footer: QuickCon14.ToolBar{
        style: ToolBarStyle {
            padding {
                left: 8
                right: 8
                top: 3
                bottom: 3
            }
            background: Rectangle {
                color: "#000000"
                implicitWidth: 100
                implicitHeight: 40
            }
        }
        RowLayout {
            anchors.fill: parent
            QuickCon14.ToolButton {
                iconSource: "imagens/ic_chevron_left_white_24px.svg"
                 Layout.fillWidth: true
            }
            QuickCon14.ToolButton {
                iconSource: "imagens/ic_apps_white_24px.svg"

                Layout.fillWidth: true
            }QuickCon14.ToolButton {
//                text: ">"
                iconSource: "imagens/ic_keyboard_white_36px.svg"
//                onClicked: {
//                    title_header = "Home"
//                    stack.push("pages/home/HomePage.qml")
//                }
                 Layout.fillWidth: true
            }

        }
    }

}



