import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
  Rectangle {
        id: progressBarCustom
        property string progressColor
        property double progressValue
        property alias progressBarCustom: progressBarCustom
        Layout.fillWidth: parent
        width: 236
        height: 55
        color: "#B0AFB0"
        radius:3
        ColumnLayout {
            width: 233
            height: 49
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            spacing: 3

            Label {
                text: "Controller\nAmbient Temperature"
                color: "#4F4F4F"
                font.pixelSize: 11
                font.bold: enabled
                Layout.alignment: Qt.AlignHCenter
                horizontalAlignment: Text.AlignHCenter
            }

            ProgressBar {
                id: control3
                value: progressValue
                Layout.alignment: Qt.AlignHCenter
                background: Rectangle {
                    implicitWidth: progressBarCustom.width - 10
                    implicitHeight: 6
                    color: "#EDF0F5"
                    radius: 3
                }

                contentItem: Item {
                    implicitWidth: progressBarCustom.width -10
                    implicitHeight: 4

                    Rectangle {
                        width: control3.visualPosition * parent.width
                        height: parent.height
                        radius: 2
                        color: progressColor
                    }
                }
            }
            RowLayout {
                Layout.leftMargin: 5
                Layout.rightMargin: 5
                width: parent.width
                spacing: 5

                Label {
                    text: "-40º C"
                    font.pixelSize: 12
                    font.bold: enabled
                    color: "#4F4F4F"
                }
                Label {
                    text: "22° of 70°C"
                    font.bold: enabled
                    font.pixelSize: 12
                    color: "#4F4F4F"
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignRight
                }
            }
        }
    }

