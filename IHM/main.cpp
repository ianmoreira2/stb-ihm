#include <QApplication>
#include <QQmlApplicationEngine>
#include <QChartView>
#include <QtCharts>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.addImportPath("/home/ian/QT/stb-ihm/IHM/imports");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
